"""Porth compiler and interpreter.

This module contain everything needed to run or compile a porth program.
This language is inspired by forth and the channel of tsoding (twitch and youtube).
"""

from importlib.metadata import PackageNotFoundError, version

try:
    __version__ = version("porth")
except PackageNotFoundError:
    __version__ = "None"

__author__ = "Guillaume Plum <guiplum@gmail.com>"
