"""Parsing specialised function.

All functions in this module are specialised around parsing files, lines and
pieces of lines (elements) into tokens.
"""

from dataclasses import astuple, dataclass
from pathlib import Path
from typing import Dict, Iterator, List, Optional, Tuple, Union, cast

from .exceptions import (
    CompilerError,
    DuplicatedMacro,
    IncompleteInclude,
    ParsingError,
    UnfinishedMacro,
)
from .tokens import OP, FileLocation, Intrinsics, Keyword, Kind, Token, TokenisedWord

KEYWORD_TABLE = {
    "if": (Keyword.IF),
    "else": (Keyword.ELSE),
    "end": (Keyword.END),
    "while": (Keyword.WHILE),
    "do": (Keyword.DO),
    "macro": (Keyword.MACRO),
    "include": (Keyword.INCLUDE),
}
INTRINSICS_TABLE = {
    "+": (OP.INTRINSIC, Intrinsics.PLUS, None),
    "-": (OP.INTRINSIC, Intrinsics.MINUS, None),
    "*": (OP.INTRINSIC, Intrinsics.MUL, None),
    "divmod": (OP.INTRINSIC, Intrinsics.DIVMOD, None),
    "=": (OP.INTRINSIC, Intrinsics.EQUAL, None),
    ">": (OP.INTRINSIC, Intrinsics.GT, None),
    "<": (OP.INTRINSIC, Intrinsics.LT, None),
    "<=": (OP.INTRINSIC, Intrinsics.LE, None),
    ">=": (OP.INTRINSIC, Intrinsics.GE, None),
    "!=": (OP.INTRINSIC, Intrinsics.NE, None),
    "<<": (OP.INTRINSIC, Intrinsics.SHL, None),
    ">>": (OP.INTRINSIC, Intrinsics.SHR, None),
    "|": (OP.INTRINSIC, Intrinsics.B_OR, None),
    "&": (OP.INTRINSIC, Intrinsics.B_AND, None),
    ".": (OP.INTRINSIC, Intrinsics.STORE, 8),
    ".64": (OP.INTRINSIC, Intrinsics.STORE, 64),
    ",": (OP.INTRINSIC, Intrinsics.LOAD, 8),
    ",64": (OP.INTRINSIC, Intrinsics.LOAD, 64),
    "dup": (OP.INTRINSIC, Intrinsics.DUP, None),
    "drop": (OP.INTRINSIC, Intrinsics.DROP, None),
    "swap": (OP.INTRINSIC, Intrinsics.SWAP, None),
    "over": (OP.INTRINSIC, Intrinsics.OVER, None),
    "mem": (OP.INTRINSIC, Intrinsics.MEM, None),
    "syscall0": (OP.INTRINSIC, Intrinsics.SYSCALL, 0),
    "syscall1": (OP.INTRINSIC, Intrinsics.SYSCALL, 1),
    "syscall2": (OP.INTRINSIC, Intrinsics.SYSCALL, 2),
    "syscall3": (OP.INTRINSIC, Intrinsics.SYSCALL, 3),
    "syscall4": (OP.INTRINSIC, Intrinsics.SYSCALL, 4),
    "syscall5": (OP.INTRINSIC, Intrinsics.SYSCALL, 5),
    "syscall6": (OP.INTRINSIC, Intrinsics.SYSCALL, 6),
    "dump": (OP.DUMP, None, None),
}


@dataclass
class Macro:
    """Store the code linked to a Macro commands.

    :param name: Name of the macro.
    :type name: str
    """

    name: str
    loc: FileLocation
    code: List[TokenisedWord]
    expanded: int = 0


def _unescape_str(text: str) -> str:
    return text.encode("utf-8").decode("unicode_escape").encode("latin-1").decode("utf-8")


def lex_word(text: str) -> Tuple[Kind, Union[int, str, Keyword]]:
    """Determine the kind of a text.

    :param text: text to lex.
    :type text: str
    :return: A tuple of kind and token value.
    """
    if text.startswith('"') and text.endswith('"'):
        return (Kind.STR, _unescape_str(text[1:-1]))
    elif text.startswith("'") and text.endswith("'"):
        return (Kind.CHAR, _unescape_str(text[1:-1]))
    elif text in KEYWORD_TABLE:
        return Kind.KEYWORD, KEYWORD_TABLE[text]
    elif text in INTRINSICS_TABLE:
        return (Kind.WORD, text)

    try:
        return (Kind.INT, int(text))
    except ValueError:
        return (Kind.WORD, text)


def parse_words(word: TokenisedWord) -> Token:
    """Transform an element into a token.

    :param word: A tokenised word.
    :type word: TokenisedWord

    :return: A token corresponding to the given element.
    :rtype: Token

    :raises ParsingError: when the token is not parsable.
    """
    if word.type == Kind.INT:
        return Token(word.loc, OP.PUSH, None, cast(int, word.value))
    elif word.type == Kind.CHAR:
        return Token(word.loc, OP.PUSH, None, ord(cast(str, word.value)))
    elif word.type == Kind.STR:
        return Token(word.loc, OP.PUSH_STR, None, cast(str, word.value))
    elif word.type == Kind.WORD and word.value in INTRINSICS_TABLE:
        return Token(word.loc, *INTRINSICS_TABLE[cast(str, word.value)])
    else:
        raise ParsingError(
            f"Invalid token: '{word.value}'.", file=word.loc.file_path, line=word.loc.line, col=word.loc.col
        )


def _next_space(data: str) -> int:
    for idx, token in filter(lambda t: t[1].isspace(), enumerate(data)):
        if token.isspace():
            return idx
    else:
        return len(data)


def _next_char(data: str, char: str) -> Optional[int]:
    for idx, token in enumerate(data):
        if token == char and data[idx - 1] != "\\":
            return idx
    else:
        return None


def lex_line(line: str) -> Iterator[Tuple[int, Tuple[Kind, Union[int, str]]]]:
    """Lex a line into the elements.

    :param line: line to lex.
    :type line: string.

    :return: The line parsed as an iterator over tuples.
    :rtype: iterator of tuples with column index and element.

    :raise ParsingError: When a string is not closed.
    """
    col_end = 0
    for col, char in enumerate(line):
        if char.isspace() or col < col_end:
            continue

        if char == '"':
            start = col + 1
            idx = _next_char(line[start:], '"')
            if idx is None:
                raise ParsingError("Unable to find the end of the string.", col=col)
            col_end = start + idx + 1
        elif char == "'":
            start = col + 1
            idx = _next_char(line[start:], "'")
            if idx is None:
                raise ParsingError("Unable to find the end of the string.", col=col)
            col_end = start + idx + 1
        else:
            col_end = col + _next_space(line[col:])
        yield col, lex_word(line[col:col_end])


def lex_file(file_path: Union[str, Path], include: List[Path]) -> Iterator[TokenisedWord]:
    """Lex the file into a list of elements.

    :param file_path: program file to parse.
    :type args: string or path object.

    :return: The file parsed as a list of elements.
    :rtype: list of TokenisedWord.

    :raise ParsingError: When an error occur during parsing.
    """
    if isinstance(file_path, str):
        file_path = Path(file_path)

    if not file_path.exists():
        for inc_path in include:
            tmp = inc_path / file_path
            if tmp.exists():
                file_path = tmp
                break
        else:
            raise FileNotFoundError(f"Unable to find file {file_path}")

    with file_path.open("r") as fich:
        for line_idx, line in enumerate(fich):
            try:
                for col, (kind, value) in lex_line(line.split("//")[0]):
                    yield TokenisedWord(kind, value, FileLocation(str(file_path), line_idx + 1, col + 1))
            except ParsingError as e:
                e.line = line_idx
                e.file = file_path
                raise e


def crossref(
    token_stream: Iterator[TokenisedWord], include: List[Path], max_macro_expansion: int = 128, max_include: int = 128
) -> List[Token]:
    """Parse the whole program and crossreference all jump instruction.

    :param program: Program to crossreference.

    :return: program with jump instructions linked together.
    """
    stack: List[int] = list()
    program: List[Token] = list()
    macros: Dict[str, Macro] = dict()
    token_stream_stack: List[Iterator[TokenisedWord]] = [token_stream]
    included_files: Dict[str, int] = dict()

    ip = 0
    while True:
        try:
            # Taking the next token from our stream:
            tword = next(token_stream_stack[-1])
        except StopIteration:
            # If the stream was empty, pop it:
            token_stream_stack.pop()
            # Then check if the stack still has a stream in waiting:
            if len(token_stream_stack) <= 0:
                # If no, the program is complete.
                break
            else:
                # Else, let us start over the loop.
                continue

        # If the token is a word and that word is in the macro list:
        if tword.type == Kind.WORD and tword.value in macros:
            if macros[cast(str, tword.value)].expanded >= max_macro_expansion:
                raise CompilerError(
                    f"Macro '{tword.value}' was expanded too many times ({macros[cast(str, tword.value)].expanded} > {max_macro_expansion})",
                    *astuple(tword.loc),
                )
            # Append the macro code to the token stream stack:
            token_stream_stack.append(iter(macros[cast(str, tword.value)].code))
            macros[cast(str, tword.value)].expanded += 1
            # And start over the loop:
            continue

        # Parse the token into an operation and an operand
        if tword.type != Kind.KEYWORD:
            parsed = parse_words(tword)
            program.append(parsed)
            ip += 1
            continue

        operand = None

        # Before adding it to the program, are we a special instruction:
        if tword.value in (Keyword.IF, Keyword.WHILE, Keyword.DO):
            # We are an instruction used as a jump point or starting one,
            # - add our current address.
            stack.append(ip)
        elif tword.value == Keyword.ELSE:
            # Else block are used both as jump target and jump starter.
            # So, first, retrieve the IF address:
            addr = stack.pop()
            # It should be an IF instructtion, else we have a problem:
            assert (
                program[addr].type == OP.IF
            ), f"{tword.loc}: Else block can only be used after an if block, not {program[addr]}."

            # Then we add the next address as a mark for the IF instruction, to jump
            # just after us.
            # +1 because we don't want the else to be executed.
            program[addr].operand = ip + 1
            # Finally, we add ourselves to the jump stack:
            stack.append(ip)
        elif tword.value == Keyword.END:
            # For the END operation, we have two behaviour:
            # - mark for a jump in a if-else block.
            # - jump back instruction in a do block.
            # First, lets deal with the jump-to behaviour:
            addr = stack.pop()
            token = program[addr]

            assert token.type in (
                OP.IF,
                OP.ELSE,
                OP.DO,
            ), f"{tword.loc}:End should close if, else and do blocks, not {token.type}."

            # We now have to do at least one thing:
            # 1. fill the do operand with the address to jump if the while is over:
            program[addr].operand = ip + 1  # +1 to avoid executing the end instruction.

            # except for DO which need one more modification:
            if token.type == OP.DO:
                # 2. we need to get the while instruction address for the end:
                addr = stack.pop()
                assert program[addr].type == OP.WHILE, f"{tword.loc}:Do instructions should only follow while."
                operand = addr
        elif tword.value == Keyword.INCLUDE:
            try:
                next_op = next(token_stream_stack[-1])
            except StopIteration:
                raise IncompleteInclude("Expecting include path.", *astuple(tword.loc))

            if next_op.type != Kind.STR:
                raise IncompleteInclude(f"Expecting include path but found {next_op}", *astuple(next_op.loc))

            try:
                if next_op.value not in included_files:
                    included_files[cast(str, next_op.value)] = 0
                included_files[cast(str, next_op.value)] += 1
                if included_files[cast(str, next_op.value)] >= max_include:
                    raise CompilerError(
                        f"File '{next_op.value}' included too many times ({included_files[cast(str, next_op.value)]} > {max_include})",
                        *astuple(tword.loc),
                    )
                token_stream_stack.append(lex_file(cast(str, next_op.value), include))
            except FileNotFoundError as e:
                raise FileNotFoundError(f"{next_op.loc.file_path}:{next_op.loc.line}:{next_op.loc.col}: {e}") from e
        elif tword.value == Keyword.MACRO:
            # Macros are special. They are not added to the code like other instructions. They are stored to be expanded later.

            # First, we get the next item from the stream:
            try:
                next_op = next(token_stream_stack[-1])
            except StopIteration:
                raise UnfinishedMacro("Unfinished macro definition.", *astuple(next_op.loc))

            # This item will be used as a name, so it must be a word:
            if next_op.type != Kind.WORD:
                raise UnfinishedMacro(
                    f"expected macro name can only be a {Kind.WORD} (found {next_op.type}).", *astuple(next_op.loc)
                )

            # And it should not be a reserved name:
            if next_op.value in INTRINSICS_TABLE:
                raise UnfinishedMacro("you are not allowed to redefined reserved words.", *astuple(next_op.loc))

            # Neither already used for another macro:
            if next_op.value in macros:
                raise DuplicatedMacro(macros[cast(str, next_op.value)], next_op.loc)

            # If everything criterion are met, we can instanciate it:
            macro_def = Macro(cast(str, next_op.value), tword.loc, list())

            macro_inside_block = 0
            for word in token_stream_stack[-1]:
                if word.type == Kind.KEYWORD and word.value == Keyword.END and macro_inside_block == 0:
                    break
                elif word.type == Kind.KEYWORD and word.value == Keyword.END:
                    macro_inside_block -= 1
                elif word.type == Kind.KEYWORD and word.value in (Keyword.IF, Keyword.WHILE):
                    macro_inside_block += 1

                macro_def.code.append(word)
            else:
                raise UnfinishedMacro("expected 'end' to close a macro definition.", *astuple(macro_def.loc))

            macros[macro_def.name] = macro_def

        if tword.type == Kind.KEYWORD and tword.value not in (Keyword.MACRO, Keyword.INCLUDE):
            program.append(Token(tword.loc, OP.from_keyword(cast(Keyword, tword.value)), None, operand))
            ip += 1

    return program


def load_from_file(
    file_path: Union[Path, str], include: List[Union[str, Path]], max_macro_expansion: int = 128, max_include: int = 128
) -> List[Token]:
    """Load and parse a porth program into a list of Token.

    This function will lex the given file and run the crossreference to correctly link
    together all instructions doing jumps.

    :param file_path: program file to parse.
    :type args: string or path object.

    :return: The file parsed as a list of token
    :rtype: list of token (tuple of :py:class OP and an optional operand).
    """
    if isinstance(file_path, str):
        file_path = Path(file_path)

    include_paths: List[Path] = list(map(lambda p: Path(p) if isinstance(p, str) else p, include))

    return crossref(lex_file(file_path, include_paths), include_paths, max_macro_expansion, max_include)
