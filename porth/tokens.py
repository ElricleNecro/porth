"""Token of the language.

Are available an enum class representing all possible kind of instructions
and a set of functions returning a Token (tuple of instruction and operand).
"""
from dataclasses import dataclass
from enum import IntEnum, auto
from typing import Optional, Union


class Kind(IntEnum):
    """Kind of token."""

    INT = auto()
    WORD = auto()
    STR = auto()
    CHAR = auto()
    KEYWORD = auto()

    def __str__(self) -> str:
        """Render the enum to a string."""
        if self == Kind.INT:
            return "int"
        elif self == Kind.WORD:
            return "word"
        elif self == Kind.STR:
            return "str"
        elif self == Kind.CHAR:
            return "char"
        elif self == Kind.KEYWORD:
            return "keyword"

        return str(self.value)

    def __repr__(self):
        """Render the enum representation."""
        return "<%s.%s>" % (self.__class__.__name__, self.name)


class Keyword(IntEnum):
    """Language keyword."""

    IF = auto()
    ELSE = auto()
    END = auto()

    WHILE = auto()
    DO = auto()

    MACRO = auto()
    INCLUDE = auto()


class Intrinsics(IntEnum):
    """Intrinsics of the language."""

    PLUS = auto()
    MINUS = auto()
    MUL = auto()
    DIVMOD = auto()

    SHR = auto()
    SHL = auto()
    B_OR = auto()
    B_AND = auto()

    EQUAL = auto()
    GT = auto()
    LT = auto()
    LE = auto()
    GE = auto()
    NE = auto()

    DUP = auto()
    DROP = auto()
    SWAP = auto()
    OVER = auto()

    MEM = auto()
    LOAD = auto()
    STORE = auto()

    SYSCALL = auto()

    def __repr__(self):
        """Render the enum representation."""
        return "<%s.%s>" % (self.__class__.__name__, self.name)


class OP(IntEnum):
    """Instruction token."""

    PUSH = auto()
    PUSH_STR = auto()
    DUMP = auto()

    INTRINSIC = auto()

    IF = auto()
    ELSE = auto()
    WHILE = auto()
    DO = auto()
    END = auto()

    def __repr__(self):
        """Render the enum representation."""
        return "<%s.%s>" % (self.__class__.__name__, self.name)

    @staticmethod
    def from_keyword(keyword: Keyword) -> "OP":
        """Convert the Keyword enum into an OP enum.

        :param keyword: Keyword value to convert.
        :type keyword: Keyword
        :returns: associated OP enum.
        :rettype: OP
        :raise ValueError: if the Keyword can't be converted.
        """
        if keyword == Keyword.IF:
            return OP.IF
        elif keyword == Keyword.ELSE:
            return OP.ELSE
        elif keyword == Keyword.WHILE:
            return OP.WHILE
        elif keyword == Keyword.DO:
            return OP.DO
        elif keyword == Keyword.END:
            return OP.END
        else:
            raise ValueError(f"Keyword '{keyword}' is not a valid operation.")


@dataclass
class FileLocation:
    """Location in a file.

    :param file_path: path to the file.
    :type file_path: str
    :param line: Line number.
    :type line: int
    :param col: Column number.
    :type col: int
    """

    file_path: str
    line: int
    col: int

    def __str__(self):
        """Render the file location into a string."""
        return f"{self.file_path}:{self.line:03}:{self.col:03}"

    def __repr__(self):
        """Render the dataclass representation."""
        return self.__str__()


Addr = int


@dataclass
class Token:
    """Token.

    :param loc: location of the token in the files sources.
    :type loc: FileLocation
    :param type: Kind of token.
    :type type: OP
    :param intrinsic: Intrinsics of the language, optional.
    :type intrinsic: Intrinsics
    :param operand: Value used by the token at execution.
    :type operand: int, Addr, str
    """

    loc: FileLocation
    type: OP
    intrinsic: Optional[Intrinsics] = None
    operand: Optional[Union[int, Addr, str]] = None


@dataclass
class TokenisedWord:
    """A tokenised word.

    :param type: Kind of token.
    :type type: Kind
    :param value: Value of the token.
    :type value: str or int
    :param loc: Where is the token in the file.
    :type loc: Tuple of file path, line and column.
    """

    type: Kind
    value: Union[str, int, Keyword]
    loc: FileLocation
