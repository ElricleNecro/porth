"""Exceptions module.

This module contain every exception the porth compiler can raise.
"""

from dataclasses import astuple
from pathlib import Path
from typing import Optional, Union


class CompilerError(Exception):
    """Base exception for errors occuring in the parser."""

    def __init__(
        self, msg: str, file: Optional[Union[str, Path]] = None, line: Optional[int] = None, col: Optional[int] = None
    ) -> None:
        """Initialise the instance.

        :param msg: Error message.
        :type msg: str
        :param file: File in which the error occured, optional.
        :type file: str or Path
        :param line: Line number of the error in the file, optional.
        :type line: int.
        :param col: Column of the error in the file, optional.
        :type col: int.
        """
        self._msg = msg
        self._file = file
        self._line = line
        self._col = col

    def __str__(self):
        """Render the instance as a string."""
        return f"{self._file}:{self._line}:{self._col}: {self._msg}"

    @property
    def file(self) -> Optional[Union[str, Path]]:
        """File in which the error occured."""
        return self._file

    @file.setter
    def file(self, file: Union[str, Path]) -> None:
        self._file = file

    @property
    def line(self) -> Optional[int]:
        """Line number of the error in the file."""
        return self._line

    @line.setter
    def line(self, line: int) -> None:
        self._line = line

    @property
    def col(self) -> Optional[int]:
        """Column of the error in the file."""
        return self._col

    @col.setter
    def col(self, col: int) -> None:
        self._col = col


class ParsingError(CompilerError):
    """Raised when there is an error when parsing an instruction."""

    pass


class IncompleteInclude(CompilerError):
    """Raised when the include command is missing an argument."""

    pass


class UnfinishedMacro(CompilerError):
    """Raised when a macro is not closed."""

    pass


class DuplicatedMacro(CompilerError):
    """Raised when redefining a macro."""

    def __init__(self, macro, loc) -> None:
        """Initialise the instance.

        :param macro: The macro object that was already defined.
        :type macro: :py:class:`porth.parser.Macro`
        :param loc: The location of the error in the file.
        :type loc: :py:class:`porth.tokens.FileLocation`
        """
        super().__init__(
            f"Macro already define at {macro.loc.file_path}:{macro.loc.line}:{macro.loc.col}.", *astuple(loc)
        )
