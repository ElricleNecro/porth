#!/usr/bin/env python3

"""Command line interface.

This module contain all methods used for th command line interface.
"""

import sys
from argparse import ArgumentParser, Namespace
from dataclasses import astuple
from os.path import join
from subprocess import call
from tempfile import TemporaryDirectory
from typing import List, cast

from . import __version__ as version
from .parser import load_from_file
from .tokens import OP, Intrinsics, Token

DUMP = """
dump:
        mov     r9, -3689348814741910323
        sub     rsp, 40
        mov     BYTE [rsp+31], 10
        lea     rcx, [rsp+30]
.L2:
        mov     rax, rdi
        lea     r8, [rsp+32]
        mul     r9
        mov     rax, rdi
        sub     r8, rcx
        shr     rdx, 3
        lea     rsi, [rdx+rdx*4]
        add     rsi, rsi
        sub     rax, rsi
        add     eax, 48
        mov     BYTE [rcx], al
        mov     rax, rdi
        mov     rdi, rdx
        mov     rdx, rcx
        sub     rcx, 1
        cmp     rax, 9
        ja      .L2
        lea     rax, [rsp+32]
        mov     edi, 1
        sub     rdx, rax
        lea     rsi, [rsp+32+rdx]
        mov     rdx, r8
        mov     rax, 1
        syscall
        add     rsp, 40
        ret
"""


def compile(args: Namespace, prog_args: List[str]) -> int:
    """Command runing the compilation stage of a porth program.

    :param args: command line parameters.
    :param prog_args: porth script arguments.
    """
    with TemporaryDirectory() as tmpdir:
        generated_asm_file = join(tmpdir if not args.debug else ".", "generated.asm")
        with open(generated_asm_file, "w") as generated:
            print("segment .text", file=generated)
            print("global _start", file=generated)

            print(DUMP, file=generated)

            print("_start:", file=generated)

            strs: List[str] = list()
            for ip, token in enumerate(
                load_from_file(
                    args.file,
                    args.include,
                    max_macro_expansion=args.max_macro_expansion,
                    max_include=args.max_include_level,
                )
            ):
                loc, op, intrinsic, operand = astuple(token)
                if op == OP.PUSH:
                    print(f"\t;; -- push {operand} --", file=generated)
                    print(f"\tmov rax,{operand}", file=generated)
                    print("\tpush rax", file=generated)
                elif op == OP.PUSH_STR:
                    print("\t;; -- push_str --", file=generated)
                    print(f"\tmov rax, {len(cast(str, operand).encode())}", file=generated)
                    print("\tpush rax", file=generated)
                    print(f"\tpush str_{len(strs)}", file=generated)

                    strs.append(cast(str, operand))
                elif op == OP.DUMP:
                    print("\t;; -- dump --", file=generated)
                    print("\tpop rdi", file=generated)
                    print("\tcall dump", file=generated)
                elif op == OP.IF:
                    print(f"\t;; -- if {operand} --", file=generated)
                    print("\tpop rax", file=generated)
                    print("\tcmp rax, 0", file=generated)
                    print(f"\tjz addr_{operand}", file=generated)
                elif op == OP.ELSE:
                    print("\t;; -- else --", file=generated)
                    print(f"jmp addr_{operand}", file=generated)
                    print(f"addr_{ip+1}:", file=generated)
                elif op == OP.WHILE:
                    print("\t;; -- while --", file=generated)
                    print(f"addr_{ip}:", file=generated)
                elif op == OP.DO:
                    print("\t;; -- do --", file=generated)
                    print("\tpop rax", file=generated)
                    print("\tcmp rax, 0", file=generated)
                    print(f"\tjz addr_{operand}", file=generated)
                elif op == OP.END:
                    print("\t;; -- end --", file=generated)
                    if operand is not None:
                        print(f"\tjmp addr_{operand}\n", file=generated)
                    print(f"addr_{ip+1}:", file=generated)
                elif op == OP.INTRINSIC:
                    if intrinsic == Intrinsics.PLUS:
                        print("\t;; -- plus --", file=generated)
                        print("\tpop rax", file=generated)
                        print("\tpop rbx", file=generated)
                        print("\tadd rax, rbx", file=generated)
                        print("\tpush rax", file=generated)
                    elif intrinsic == Intrinsics.MINUS:
                        print("\t;; -- minus --", file=generated)
                        print("\tpop rbx", file=generated)
                        print("\tpop rax", file=generated)
                        print("\tsub rax, rbx", file=generated)
                        print("\tpush rax", file=generated)
                    elif intrinsic == Intrinsics.MUL:
                        print("\t;; -- mul --", file=generated)
                        print("\tpop rbx", file=generated)
                        print("\tpop rax", file=generated)
                        print("\tmul rbx", file=generated)
                        print("\tpush rax", file=generated)
                    elif intrinsic == Intrinsics.DIVMOD:
                        print("\t;; -- mod --", file=generated)
                        print("\txor rdx, rdx", file=generated)
                        print("\tpop rbx", file=generated)
                        print("\tpop rax", file=generated)
                        print("\tdiv rbx", file=generated)
                        print("\tpush rax", file=generated)
                        print("\tpush rdx", file=generated)
                    elif intrinsic == Intrinsics.SHL:
                        print("\t;; -- shl --", file=generated)
                        print("\tpop rcx", file=generated)
                        print("\tpop rax", file=generated)
                        print("\tshl rax, cl", file=generated)
                        print("\tpush rax", file=generated)
                    elif intrinsic == Intrinsics.SHR:
                        print("\t;; -- shr --", file=generated)
                        print("\tpop rcx", file=generated)
                        print("\tpop rax", file=generated)
                        print("\tshr rax, cl", file=generated)
                        print("\tpush rax", file=generated)
                    elif intrinsic == Intrinsics.B_OR:
                        print("\t;; -- bitwise or --", file=generated)
                        print("\tpop rbx", file=generated)
                        print("\tpop rax", file=generated)
                        print("\tor rax, rbx", file=generated)
                        print("\tpush rax", file=generated)
                    elif intrinsic == Intrinsics.B_AND:
                        print("\t;; -- bitwise and --", file=generated)
                        print("\tpop rbx", file=generated)
                        print("\tpop rax", file=generated)
                        print("\tand rax, rbx", file=generated)
                        print("\tpush rax", file=generated)
                    elif intrinsic == Intrinsics.EQUAL:
                        print("\t;; -- equal --", file=generated)
                        print("\tmov rcx, 0", file=generated)
                        print("\tmov rdx, 1", file=generated)
                        print("\tpop rax", file=generated)
                        print("\tpop rbx", file=generated)
                        print("\tcmp rax, rbx", file=generated)
                        print("\tcmove rcx, rdx", file=generated)
                        print("\tpush rcx", file=generated)
                    elif intrinsic == Intrinsics.GT:
                        print("\t;; -- gt --", file=generated)
                        print("\tmov rcx, 0", file=generated)
                        print("\tmov rdx, 1", file=generated)
                        print("\tpop rbx", file=generated)
                        print("\tpop rax", file=generated)
                        print("\tcmp rax, rbx", file=generated)
                        print("\tcmovg rcx, rdx", file=generated)
                        print("\tpush rcx", file=generated)
                    elif intrinsic == Intrinsics.LT:
                        print("\t;; -- lt --", file=generated)
                        print("\tmov rcx, 0", file=generated)
                        print("\tmov rdx, 1", file=generated)
                        print("\tpop rbx", file=generated)
                        print("\tpop rax", file=generated)
                        print("\tcmp rax, rbx", file=generated)
                        print("\tcmovl rcx, rdx", file=generated)
                        print("\tpush rcx", file=generated)
                    elif intrinsic == Intrinsics.NE:
                        print("\t;; -- gt --", file=generated)
                        print("\tmov rcx, 0", file=generated)
                        print("\tmov rdx, 1", file=generated)
                        print("\tpop rbx", file=generated)
                        print("\tpop rax", file=generated)
                        print("\tcmp rax, rbx", file=generated)
                        print("\tcmovne rcx, rdx", file=generated)
                        print("\tpush rcx", file=generated)
                    elif intrinsic == Intrinsics.GE:
                        print("\t;; -- gt --", file=generated)
                        print("\tmov rcx, 0", file=generated)
                        print("\tmov rdx, 1", file=generated)
                        print("\tpop rbx", file=generated)
                        print("\tpop rax", file=generated)
                        print("\tcmp rax, rbx", file=generated)
                        print("\tcmovge rcx, rdx", file=generated)
                        print("\tpush rcx", file=generated)
                    elif intrinsic == Intrinsics.LE:
                        print("\t;; -- gt --", file=generated)
                        print("\tmov rcx, 0", file=generated)
                        print("\tmov rdx, 1", file=generated)
                        print("\tpop rbx", file=generated)
                        print("\tpop rax", file=generated)
                        print("\tcmp rax, rbx", file=generated)
                        print("\tcmovle rcx, rdx", file=generated)
                        print("\tpush rcx", file=generated)
                    elif intrinsic == Intrinsics.DUP:
                        print("\t;; -- dup --", file=generated)
                        print("\tpop rax", file=generated)
                        print("\tpush rax", file=generated)
                        print("\tpush rax", file=generated)
                    elif intrinsic == Intrinsics.DROP:
                        print("\t;; -- drintrinsic --", file=generated)
                        print("\tpop rax", file=generated)
                    elif intrinsic == Intrinsics.SWAP:
                        print("\t;; -- swap --", file=generated)
                        print("\tpop rax", file=generated)
                        print("\tpop rbx", file=generated)
                        print("\tpush rax", file=generated)
                        print("\tpush rbx", file=generated)
                    elif intrinsic == Intrinsics.OVER:
                        print("\t;; -- over --", file=generated)
                        print("\tpop rax", file=generated)
                        print("\tpop rbx", file=generated)
                        print("\tpush rbx", file=generated)
                        print("\tpush rax", file=generated)
                        print("\tpush rbx", file=generated)
                    elif intrinsic == Intrinsics.MEM:
                        print("\t;; -- mem --", file=generated)
                        print("\tpush mem", file=generated)
                    elif intrinsic == Intrinsics.LOAD:
                        print("\t;; -- load --", file=generated)
                        print("\tpop rax", file=generated)
                        print("\txor rbx, rbx", file=generated)
                        if token.operand == 8:
                            print("\tmov bl, [rax]", file=generated)
                        elif token.operand in (16, 32, 64):
                            print("\tmov rbx, [rax]", file=generated)
                        print("\tpush rbx", file=generated)
                    elif intrinsic == Intrinsics.STORE:
                        print("\t;; -- store --", file=generated)
                        print("\tpop rbx", file=generated)
                        print("\tpop rax", file=generated)
                        if token.operand == 8:
                            print("\tmov [rax], bl", file=generated)
                        elif token.operand in (16, 32, 64):
                            print("\tmov [rax], rbx", file=generated)
                    elif intrinsic == Intrinsics.SYSCALL:
                        print("\t;; -- syscall --", file=generated)
                        print("\tpop rax", file=generated)
                        if operand == 1:
                            print("\tpop rdi", file=generated)
                        elif operand == 3:
                            print("\tpop rdi", file=generated)
                            print("\tpop rsi", file=generated)
                            print("\tpop rdx", file=generated)
                        else:
                            raise ValueError("Syscalls with other than 1 or 3 arguments are not yet supported.")
                        print("\tsyscall", file=generated)
                        print("\tpush rax", file=generated)
                    else:
                        assert False, f"Unreachable intrinsic: {intrinsic}"
                else:
                    assert False, f"Unreachable instruction: {op}"

            print("\tmov rax, 0x3c", file=generated)
            print("\tmov rdi, 0", file=generated)
            print("\tsyscall", file=generated)

            print("segment .bss", file=generated)
            print(f"\tmem: resb 0x{args.allocated_memory:x}", file=generated)

            print("segment .data", file=generated)
            for idx, s in enumerate(strs):
                print(f"\tstr_{idx}: db {','.join(map(hex, list(s.encode())))}", file=generated)

        if (return_code := call(["nasm", "-felf64", "-o", f"{generated_asm_file}.o", generated_asm_file])) != 0:
            return return_code

        if (return_code := call(["ld", "-o", args.output_binary, f"{generated_asm_file}.o"])) != 0:
            return return_code

    if args.run:
        return call([f"./{args.output_binary}"] + prog_args)

    return 0


def _step(token: Token) -> int:
    pass


def run(args: Namespace, prog_args: List[str]) -> int:
    """Command interpreting a porth program.

    :param args: command line parameters.
    """
    prog_args = [args.file] + prog_args

    mem: bytearray = bytearray(args.allocated_memory)
    mem_end = args.allocated_memory
    stack: List[int] = list()
    for arg in reversed(prog_args):
        stack.append(len(mem))
        mem += f"{arg}\0".encode()
    stack.append(len(prog_args))

    program = load_from_file(
        args.file, args.include, max_macro_expansion=args.max_macro_expansion, max_include=args.max_include_level
    )
    ip = 0
    instructions_count = 0

    while ip < len(program):
        token = program[ip]
        ip += 1
        instructions_count += 1

        if token.type == OP.PUSH:
            if token.operand is None:
                raise ValueError(f"{token.loc}: PUSH token.operand should not be None.")
            stack.append(cast(int, token.operand))
        elif token.type == OP.PUSH_STR:
            str_encoded = cast(str, token.operand).encode()
            str_len = len(str_encoded)
            addr = mem_end - str_len - 1

            stack.append(str_len)
            stack.append(addr)

            for idx, val in enumerate(str_encoded):
                mem[addr + idx] = val
        elif token.type == OP.IF:
            a = stack.pop()
            if a == 0:
                # Jump to associated end.
                if token.operand is not None and isinstance(token.operand, int):
                    ip = token.operand
                else:
                    raise ValueError(
                        f"{token.loc}: Problem occured during crossref stage, an if instruction as nowhere to jump."
                    )
        elif token.type == OP.ELSE:
            if token.operand is not None and isinstance(token.operand, int):
                ip = token.operand
            else:
                raise ValueError(
                    f"{token.loc}: Problem occured during crossref stage, an if instruction as nowhere to jump."
                )
        elif token.type == OP.WHILE:
            pass
        elif token.type == OP.DO:
            a = stack.pop()
            if a == 0:
                if token.operand is not None and isinstance(token.operand, int):
                    ip = token.operand
                else:
                    raise ValueError(
                        f"{token.loc}: Problem occured during crossref stage, an if instruction as nowhere to jump."
                    )
        elif token.type == OP.END:
            if token.operand is not None and isinstance(token.operand, int):
                ip = token.operand
        elif token.type == OP.DUMP:
            print(stack.pop())
        elif token.type == OP.INTRINSIC:
            if token.intrinsic == Intrinsics.PLUS:
                a, b = stack.pop(), stack.pop()
                stack.append(a + b)
            elif token.intrinsic == Intrinsics.MINUS:
                a, b = stack.pop(), stack.pop()
                stack.append(b - a)
            elif token.intrinsic == Intrinsics.MUL:
                a, b = stack.pop(), stack.pop()
                stack.append(b * a)
            elif token.intrinsic == Intrinsics.DIVMOD:
                a, b = stack.pop(), stack.pop()
                stack.append(b // a)
                stack.append(b % a)
            elif token.intrinsic == Intrinsics.SHL:
                b, a = stack.pop(), stack.pop()
                stack.append(a << b)
            elif token.intrinsic == Intrinsics.SHR:
                b, a = stack.pop(), stack.pop()
                stack.append(a >> b)
            elif token.intrinsic == Intrinsics.B_OR:
                b, a = stack.pop(), stack.pop()
                stack.append(a | b)
            elif token.intrinsic == Intrinsics.B_AND:
                b, a = stack.pop(), stack.pop()
                stack.append(a & b)
            elif token.intrinsic == Intrinsics.EQUAL:
                a, b = stack.pop(), stack.pop()
                stack.append(1 if a == b else 0)
            elif token.intrinsic == Intrinsics.GT:
                b, a = stack.pop(), stack.pop()
                stack.append(1 if a > b else 0)
            elif token.intrinsic == Intrinsics.LT:
                b, a = stack.pop(), stack.pop()
                stack.append(1 if a < b else 0)
            elif token.intrinsic == Intrinsics.NE:
                a, b = stack.pop(), stack.pop()
                stack.append(1 if a != b else 0)
            elif token.intrinsic == Intrinsics.GE:
                b, a = stack.pop(), stack.pop()
                stack.append(1 if a >= b else 0)
            elif token.intrinsic == Intrinsics.LE:
                b, a = stack.pop(), stack.pop()
                stack.append(1 if a <= b else 0)
            elif token.intrinsic == Intrinsics.DUP:
                stack.append(stack[-1])
            elif token.intrinsic == Intrinsics.DROP:
                stack.pop()
            elif token.intrinsic == Intrinsics.SWAP:
                a, b = stack.pop(), stack.pop()
                stack.append(a)
                stack.append(b)
            elif token.intrinsic == Intrinsics.OVER:
                stack.append(stack[-2])
            elif token.intrinsic == Intrinsics.MEM:
                stack.append(0)
            elif token.intrinsic == Intrinsics.LOAD:
                addr = stack.pop()
                size = cast(int, token.operand)
                _bytes = bytearray(size // 8)
                for offset in range(size // 8):
                    _bytes[offset] = mem[addr + offset]
                stack.append(int.from_bytes(_bytes, byteorder="little"))
            elif token.intrinsic == Intrinsics.STORE:
                size = cast(int, token.operand)
                value, addr = stack.pop(), stack.pop()
                bytes_value = value.to_bytes(size // 8, byteorder="little", signed=(value < 0))

                for offset in range(size // 8):
                    mem[addr + offset] = bytes_value[offset]
            elif token.intrinsic == Intrinsics.SYSCALL:
                syscall_number = stack.pop()
                if token.operand is None or not isinstance(token.operand, int):
                    raise ValueError(
                        f"{token.loc}: There is no token.operand for the syscall instruction, something went wrong during parsing."
                    )
                syscall_args = [stack.pop() for _ in range(token.operand)]

                if syscall_number == 1:
                    fd = syscall_args[0]
                    buf = syscall_args[1]
                    count = syscall_args[2]
                    end = buf + count

                    if fd != 1 and fd != 2:
                        raise ValueError(f"{token.loc}: Unknown file descriptor: {fd}")

                    print(mem[buf:end].decode(), end="", file=sys.stdout if fd == 1 else sys.stderr)
                    stack.append(count)
                elif syscall_number == 60:
                    exit_number = syscall_args[0]

                    return exit_number
                else:
                    raise ValueError(f"{token.loc}: Unsupported syscall {syscall_number}")
            else:
                assert False, f"{token.loc}: Unreachable token.intrinsic {token.intrinsic}, {program}."
        else:
            assert False, f"{token.loc}: Unreachable {token.type}, {program}."

        args.inspect(
            f"{token.loc}:0x{ip-1:04X}:",
            token.intrinsic if token.type == OP.INTRINSIC else token.type,
            token.operand,
            stack,
        )
        if args.limit is not None and instructions_count >= args.limit:
            break

    return 0


def _common_args(parser):
    parser.add_argument(
        "-a", "--allocated-memory", type=int, default=5 * 2 << 19, help="Memory to allocate for the program."
    )

    parser.add_argument(
        "-I", "--include", default=["stdlib"], action="append", help="Where to search for included files."
    )

    parser.add_argument(
        "--max-macro-expansion", default=128, type=int, help="How many times a macro can be expanded at maximum."
    )

    parser.add_argument(
        "--max-include-level", default=128, type=int, help="How many times a file can be included at maximum."
    )


def nop(*args, **kwargs):
    """Do nothing."""
    pass


def _parse_args():
    parser = ArgumentParser()
    parser.add_argument("--version", action="version", version=version)

    subparser = parser.add_subparsers(title="Commands", required=True, help="Run or compile a porth program.")
    run_parser = subparser.add_parser("run", description="Execute the porth program.")
    run_parser.set_defaults(func=run)
    _common_args(run_parser)
    run_parser.add_argument("file", type=str, help="Program to run.")
    run_parser.add_argument("--limit", type=int, default=None, help="Maximum number of instruction to run.")
    run_parser.add_argument(
        "--inspect", default=nop, const=print, action="store_const", help="Inspect instruction and stack."
    )

    com_parser = subparser.add_parser("compile", description="Compile the program to a binary.")
    com_parser.set_defaults(func=compile)
    _common_args(com_parser)
    com_parser.add_argument("file", type=str, help="Program to compile.")
    com_parser.add_argument(
        "-o",
        "--output-binary",
        type=str,
        default="a.out",
        help="Program to compile.",
    )
    com_parser.add_argument(
        "-r", "--run", action="store_true", help="Run the program if the compilation is successful."
    )
    com_parser.add_argument("--debug", action="store_true", help="Don't use temporary file for the generated assembly.")

    return parser.parse_known_args()


def main() -> int:
    """Entry point for the command line interface of the porth interpreter and compiler."""
    args, prog_args = _parse_args()
    if "--" in prog_args:
        prog_args.remove("--")
    return args.func(args, prog_args)


if "__main__" == __name__:
    main()
