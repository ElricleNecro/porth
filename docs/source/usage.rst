Usage
=====

Goal
----
My main goal with this project is to get better at writing compilers and interpreters.
As of now, this language is still very young and basic in its implementation, but there will
be optimisation pass and other advanced features like that coming.

How to use
----------

There is two mode: interpreted and compiled. To run a porth program without compiling it, run:

.. code-block:: bash

        porth run hello.porth

If you prefer to have a native binary (Linux only as of the writing of this document), run:

.. code-block:: bash

        porth compile -o hello hello.porth
        ./hello

A typical porth program look like:

.. code-block:: 

   35 34 + 69 = if
       0 if
           23 .
       else
           69 .
       end
   else
       420 .
   end

   10 while dup 0 > do
       dup .
       1 -
   end

