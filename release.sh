#!/usr/bin/env sh

VERSION=$1
PREVIOUS_VERSION=${2:-$(git tag | tail -1)}

cat > new_tag.md <<EOF
# Version ${VERSION}

## New Feature
$(git log --pretty="- %s" ${PREVIOUS_VERSION}.. | grep feat | sed -e 's/feat\((.*)\)*: //')

## Fixes
$(git log --pretty="- %s" ${PREVIOUS_VERSION}.. | grep fix | sed -e 's/fix\((.*)\)*: //')

## Other
$(git log --pretty="- %s" ${PREVIOUS_VERSION}.. | grep -vE 'fix|feat' | sed -E -e 's/(fix|feat|refacto|style|doc)(\(.*\))*: //')

EOF
if [[ -f CHANGELOG.md ]]
then
	mv CHANGELOG.md old_CHANGELOG.md
	cat new_tag.md old_CHANGELOG.md >> CHANGELOG.md
	rm old_CHANGELOG.md
fi

git add CHANGELOG.md
git commit -m "Updating change logs."
git tag -s -F new_tag.md ${VERSION}

rm new_tag.md
