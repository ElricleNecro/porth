" Vim syntax file
" Language: Basm

" Usage Instructions
" Put this file in .vim/syntax/porth.vim
" and add in your .vimrc file the next line:
" autocmd BufRead,BufNewFile *.porth set filetype=porth

if exists("b:current_syntax")
  finish
endif

syntax keyword porthTodos TODO XXX FIXME NOTE

" Language keywords
syntax keyword porthKeywords dump
syntax keyword porthKeywords if else while do end
syntax keyword porthKeywords + - * % \>\> \<\< \| \& = \< \> \<= \>= !=
syntax keyword porthKeywords dup drop swap over
syntax keyword porthKeywords mem . ,
syntax keyword porthKeywords syscall1 syscall2 syscall3 syscall4 syscall5 syscall 6
syntax keyword porthDirective macro include

" Comments
syntax region porthCommentLine start="//" end="$"   contains=porthTodos

" Numbers
syntax match porthDecInt display "\<[0-9][0-9_]*"
syntax match porthHexInt display "\<0[xX][0-9a-fA-F][0-9_a-fA-F]*"
syntax match porthFloat  display "\<[0-9][0-9_]*\%(\.[0-9][0-9_]*\)"
syntax match porthOctInt  display "0[0-7][0-7]\+"
syntax match porthHexInt  display "0[xX][0-9a-fA-F]\+"
syntax match porthBinInt  display "0[bB][0-1]*"

" Strings
syntax region porthString start=/\v"/ skip=/\v\\./ end=/\v"/
syntax region porthString start=/\v'/ skip=/\v\\./ end=/\v'/

" Set highlights
highlight default link porthTodos Todo
highlight default link porthKeywords Keyword
highlight default link porthCommentLine Comment
highlight default link porthDirective PreProc
highlight default link porthDecInt Number
highlight default link porthHexInt Number
highlight default link porthOctInt Number
highlight default link porthHexInt Number
highlight default link porthBinInt Number
highlight default link porthFloat Float
highlight default link porthString String

let b:current_syntax = "porth"
