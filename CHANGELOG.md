# Version v0.3

## New Feature
- added as examples solutions to some euler problem.
- added a macro to increment a 64bit number.
- added an option to debug generated assembly to the compile command.
- div and mod are now one instructions, closer to the CPU one, with new macro replacing / and %.
- added store64 and load64 to the standard library.
- added vim syntax file.
- added support for not equal, less or equal and greater or equal operations.
- added support for characters literals.
- added stdin, stderr and sys_read macros.
- added multiplication and modulo operator.
- added include file directive support.
- Allowed if and while block in macros.
- adapted rule-110 to use macros.
- added macro support.
- now supporting string litterals.
- optimising rule 110 example.
- added rule 110 example.
- added over instruction.
- added swap and lt instructions.
- added bitwise operation: shr, shl, &, |
- added a shortcut to duplicate the las two values on the stack.
- added drop instruction.
- added syscall support for a limited set of actual syscall.

## Fixes
- fixing doc-lint errors.
- fixing unit tests.
- use a mov to push to take big integer into account.
- some examples stopped working with the few last patches.
- bad mixing between enum values.
- fixing a few bugs and adding more info to find a bug in the run command.
- fixing name and tools errors.
- an if which should have been an elif.
- correct type annotation and added cast when necessary.
- corrected unit tests.
- showing opcode when an unreachable code is reach
- adding results of syscall to the stack.
- allowing to put escaped " in strings.
- fixing token documentation.
- added `./` in front of the binary for execution with the `-r` option.
- printing memory in the example.

## Other
- Added script to automate release.
- Added changelog for previous tags.
- moving out of poetry, too much problems.
- applied formatter and mypy.
- completing the stdlib and 2dup is now a macro.
- continuing refactoring operations.
- separating keyword token from instruction.
- Update .gitlab-ci.yml
- Update .gitlab-ci.yml
- applied formmatter.
- applied linter.
- run formatter.
- changed numbering.
- updating the wip example.
- ignoring dev files.
- making 05-hello-world.porth more generic.
- ignoring build dir.
- added examples.
- separated a test into 3.

# Version v0.2

## New Feature
- added support to the run command for mem, load and store.
- added load and store instructions.
- added an instruction, compile only, to prepare memory access.

## Fixes
- append, not push.
- bad type annotation, bad.
- fixing linter errors.
- allowing commands to return an error code.
- renamed '.' to 'dump'.

## Other
- reformatted by black.
- bump version.
- replacing autopep8 and pycodestyle by black and flake8 due to some bugs.

# Version v0.0.1a

## New Feature
- added package deployment.
- cleaning doc.
- added doc linting stage.
- configuring CI.
- implemented while loop.
- added greater instruction.
- missing dup import.
- added dup instruction.
- updated example with else.
- added assembly generation for if (corrected) and else instructions.
- added support for else instruction.
- added if-block support.
- refactor a little bit plus added equal operator.

## Fixes
- fixing warnings and errors.
- fixing mypy errors.

## Other
- removed unused file.
- added LICENSE
- added unit tests.
- added documentation.
- renamed files to more sensitive names
- added nested if example.
- Added some errors check and enhancing the loops variables.
- File arguments should now be strings.
- Improving parsing and error reporting.
- Ignoring dev files.
- Adding poetry support.
- Added readme skeleton.
- Updating configuration.
- linting.
- Added example.
- Compiling from a basic language.
