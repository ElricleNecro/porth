from pathlib import Path
from unittest.mock import Mock

from porth.__main__ import compile, nop, run


def test_run():
    examples = Path("examples")

    for example in examples.iterdir():
        if not example.is_file():
            continue

        args = Mock()
        args.file = example
        args.allocated_memory = 128
        args.include = [Path("stdlib")]
        args.max_macro_expansion = 128
        args.max_include_level = 128
        args.inspect = nop
        args.limit = None

        run(args, list())


def test_compile():
    examples = Path("examples")

    for example in examples.iterdir():
        if not example.is_file():
            continue

        args = Mock()
        args.file = example
        args.allocated_memory = 128
        args.include = [Path("stdlib")]
        args.max_macro_expansion = 128
        args.max_include_level = 128
        args.run = True
        args.output_binary = "a.out"
        args.debug = False

        compile(args, list())


def test_compile_exit_2():
    file = Path("tests/input/exit-2.porth")

    args = Mock()
    args.file = file
    args.allocated_memory = 128
    args.include = [Path("stdlib")]
    args.max_macro_expansion = 128
    args.max_include_level = 128
    args.run = True
    args.output_binary = "a.out"
    args.debug = False

    assert compile(args, list()) == 2


def test_compile_exit_0():
    file = Path("tests/input/exit-0.porth")

    args = Mock()
    args.file = file
    args.allocated_memory = 128
    args.include = [Path("stdlib")]
    args.max_macro_expansion = 128
    args.max_include_level = 128
    args.run = True
    args.output_binary = "a.out"
    args.debug = False

    assert compile(args, list()) == 0


def test_run_exit_2():
    file = Path("tests/input/exit-2.porth")

    args = Mock()
    args.file = file
    args.allocated_memory = 128
    args.include = [Path("stdlib")]
    args.max_macro_expansion = 128
    args.max_include_level = 128
    args.inspect = nop
    args.limit = None

    assert run(args, list()) == 2


def test_run_exit_0():
    file = Path("tests/input/exit-0.porth")

    args = Mock()
    args.file = file
    args.allocated_memory = 128
    args.include = [Path("stdlib")]
    args.max_macro_expansion = 128
    args.max_include_level = 128
    args.inspect = nop
    args.limit = None

    assert run(args, list()) == 0
