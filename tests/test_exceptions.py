from unittest.mock import Mock

from porth.exceptions import CompilerError, DuplicatedMacro
from porth.tokens import FileLocation


def test_compiler_error():
    err = CompilerError("Dummy", "toto.s", 10, 90)

    assert str(err) == "toto.s:10:90: Dummy"

    assert err.file == "toto.s"
    err.file = "object.o"
    assert err.file == "object.o"

    assert err.line == 10
    err.line = 100
    assert err.line == 100

    assert err.col == 90
    err.col = 0
    assert err.col == 0


def test_duplicated_macro():
    macro = Mock()
    macro.loc = FileLocation("toto.s", 10, 90)

    err = DuplicatedMacro(macro, FileLocation("null.s", 24, 3))

    assert str(err) == "null.s:24:3: Macro already define at toto.s:10:90."
