from pathlib import Path

from pytest import raises

from porth.exceptions import CompilerError, ParsingError
from porth.parser import FileLocation, TokenisedWord, lex_file, lex_line, load_from_file, parse_words
from porth.tokens import OP, Intrinsics, Kind, Token


def test_parse_int():
    inputs = [
        34,
    ]
    loc = FileLocation("dummy", 33, 2)
    expected = [
        Token(loc, OP.PUSH, None, 34),
    ]

    for inp, exp in zip(inputs, expected):
        assert parse_words(TokenisedWord(Kind.INT, inp, loc)) == exp, f"Token for {inp} was not parsed as expected."


def test_parse_str():
    inputs = [
        """Héhé\n""",
    ]
    loc = FileLocation("dummy", 33, 2)
    expected = [
        Token(loc, OP.PUSH_STR, None, "Héhé\n"),
    ]

    for inp, exp in zip(inputs, expected):
        assert parse_words(TokenisedWord(Kind.STR, inp, loc)) == exp, f"Token for {inp} was not parsed as expected."


def test_parse_words():
    inputs = [
        "+",
        "-",
        "dump",
        "=",
        ">",
        "<",
        "<<",
        ">>",
        "&",
        "|",
        "dup",
        "drop",
        "swap",
        "over",
        "mem",
        ".",
        ",",
        ".64",
        ",64",
        "syscall1",
        "syscall3",
    ]
    loc = FileLocation("dummy", 33, 2)
    expected = [
        Token(loc, OP.INTRINSIC, Intrinsics.PLUS, None),
        Token(loc, OP.INTRINSIC, Intrinsics.MINUS, None),
        Token(loc, OP.DUMP, None, None),
        Token(loc, OP.INTRINSIC, Intrinsics.EQUAL, None),
        Token(loc, OP.INTRINSIC, Intrinsics.GT, None),
        Token(loc, OP.INTRINSIC, Intrinsics.LT, None),
        Token(loc, OP.INTRINSIC, Intrinsics.SHL, None),
        Token(loc, OP.INTRINSIC, Intrinsics.SHR, None),
        Token(loc, OP.INTRINSIC, Intrinsics.B_AND, None),
        Token(loc, OP.INTRINSIC, Intrinsics.B_OR, None),
        Token(loc, OP.INTRINSIC, Intrinsics.DUP, None),
        Token(loc, OP.INTRINSIC, Intrinsics.DROP, None),
        Token(loc, OP.INTRINSIC, Intrinsics.SWAP, None),
        Token(loc, OP.INTRINSIC, Intrinsics.OVER, None),
        Token(loc, OP.INTRINSIC, Intrinsics.MEM, None),
        Token(loc, OP.INTRINSIC, Intrinsics.STORE, 8),
        Token(loc, OP.INTRINSIC, Intrinsics.LOAD, 8),
        Token(loc, OP.INTRINSIC, Intrinsics.STORE, 64),
        Token(loc, OP.INTRINSIC, Intrinsics.LOAD, 64),
        Token(loc, OP.INTRINSIC, Intrinsics.SYSCALL, 1),
        Token(loc, OP.INTRINSIC, Intrinsics.SYSCALL, 3),
    ]

    for inp, exp in zip(inputs, expected):
        assert parse_words(TokenisedWord(Kind.WORD, inp, loc)) == exp, f"Token for {inp} was not parsed as expected."


def test_parse_words_error_1():
    with raises(ParsingError) as excep:
        parse_words(TokenisedWord(Kind.WORD, "3.3", FileLocation("dummy", 10, 32)))
    assert "dummy:10:32: Invalid token: '3.3'." in str(excep.value)


def test_parse_words_error_2():
    with raises(ParsingError) as excep:
        parse_words(TokenisedWord(Kind.WORD, "raté", FileLocation("dummy", 10, 32)))
    assert """dummy:10:32: Invalid token: 'raté'.""" in str(excep.value)


def test_lex_line_empty():
    expected = []
    results = list(lex_line(""))

    assert len(expected) == len(results), "lex_line didn't return the expected number of token."

    for res, exp in zip(results, expected):
        assert res == exp, "A token was wrong."


def test_lex_line_single_int():
    expected = [(0, (Kind.INT, 3))]
    results = list(lex_line("3"))

    assert len(expected) == len(results), "lex_line didn't return the expected number of token."

    for res, exp in zip(results, expected):
        assert res == exp, "A token was wrong."


def test_lex_line_single_word():
    expected = [(0, (Kind.WORD, "over"))]
    results = list(lex_line("over"))

    assert len(expected) == len(results), "lex_line didn't return the expected number of token."

    for res, exp in zip(results, expected):
        assert res == exp, "A token was wrong."


def test_lex_line_many():
    expected = [(0, (Kind.INT, 3)), (2, (Kind.INT, 56)), (5, (Kind.WORD, "+")), (7, (Kind.WORD, "dump"))]
    results = list(lex_line("3 56 + dump"))

    assert len(expected) == len(results), "lex_line didn't return the expected number of token."

    for res, exp in zip(results, expected):
        assert res == exp, "A token was wrong."


def test_lex_file_single_line():
    fpath = "tests/input/single_line.porth"
    expected = [
        TokenisedWord(Kind.INT, 35, FileLocation(fpath, 1, 1)),
        TokenisedWord(Kind.INT, 34, FileLocation(fpath, 1, 4)),
        TokenisedWord(Kind.WORD, "+", FileLocation(fpath, 1, 7)),
        TokenisedWord(Kind.INT, 69, FileLocation(fpath, 1, 9)),
        TokenisedWord(Kind.WORD, "=", FileLocation(fpath, 1, 12)),
        TokenisedWord(Kind.WORD, "dump", FileLocation(fpath, 1, 14)),
    ]
    results = list(lex_file(fpath, [Path("stdlib")]))

    assert len(expected) == len(results), "lex_line didn't return the expected number of token."

    for res, exp in zip(results, expected):
        assert res == exp, "A token was wrong."


def test_lex_file_two_line():
    fpath = "tests/input/two_line.porth"
    expected = [
        TokenisedWord(Kind.INT, 35, FileLocation(fpath, 1, 1)),
        TokenisedWord(Kind.INT, 34, FileLocation(fpath, 1, 4)),
        TokenisedWord(Kind.WORD, "+", FileLocation(fpath, 1, 7)),
        TokenisedWord(Kind.INT, 69, FileLocation(fpath, 1, 9)),
        TokenisedWord(Kind.WORD, "=", FileLocation(fpath, 1, 12)),
        TokenisedWord(Kind.WORD, "dump", FileLocation(fpath, 1, 14)),
        TokenisedWord(Kind.INT, 35, FileLocation(fpath, 2, 1)),
        TokenisedWord(Kind.INT, 34, FileLocation(fpath, 2, 4)),
        TokenisedWord(Kind.WORD, "+", FileLocation(fpath, 2, 7)),
        TokenisedWord(Kind.INT, 69, FileLocation(fpath, 2, 9)),
        TokenisedWord(Kind.WORD, "=", FileLocation(fpath, 2, 12)),
        TokenisedWord(Kind.WORD, "dump", FileLocation(fpath, 2, 14)),
    ]
    results = list(lex_file(fpath, [Path("stdlib")]))

    assert len(expected) == len(results), "lex_line didn't return the expected number of token."

    for res, exp in zip(results, expected):
        assert res == exp, "A token was wrong."


def test_lex_file_two_line_with_comments():
    fpath = "tests/input/two_line_with_comments.porth"
    expected = [
        TokenisedWord(Kind.INT, 35, FileLocation(fpath, 2, 1)),
        TokenisedWord(Kind.INT, 34, FileLocation(fpath, 2, 4)),
        TokenisedWord(Kind.WORD, "+", FileLocation(fpath, 2, 7)),
        TokenisedWord(Kind.INT, 69, FileLocation(fpath, 2, 9)),
        TokenisedWord(Kind.WORD, "=", FileLocation(fpath, 2, 12)),
        TokenisedWord(Kind.WORD, "dump", FileLocation(fpath, 2, 14)),
        TokenisedWord(Kind.INT, 35, FileLocation(fpath, 3, 1)),
        TokenisedWord(Kind.INT, 34, FileLocation(fpath, 3, 4)),
        TokenisedWord(Kind.WORD, "+", FileLocation(fpath, 3, 7)),
        TokenisedWord(Kind.INT, 69, FileLocation(fpath, 3, 9)),
        TokenisedWord(Kind.WORD, "=", FileLocation(fpath, 3, 12)),
        TokenisedWord(Kind.WORD, "dump", FileLocation(fpath, 3, 14)),
    ]
    results = list(lex_file(fpath, [Path("stdlib")]))

    assert len(expected) == len(results), "lex_line didn't return the expected number of token."

    for res, exp in zip(results, expected):
        assert res == exp, "A token was wrong."


def test_lex_file_write_mem():
    fpath = "tests/input/write_mem.porth"
    expected = [
        TokenisedWord(Kind.WORD, "mem", FileLocation(fpath, 1, 1)),
        TokenisedWord(Kind.INT, 0, FileLocation(fpath, 1, 5)),
        TokenisedWord(Kind.WORD, "+", FileLocation(fpath, 1, 7)),
        TokenisedWord(Kind.INT, 97, FileLocation(fpath, 1, 9)),
        TokenisedWord(Kind.WORD, ".", FileLocation(fpath, 1, 12)),
        TokenisedWord(Kind.WORD, "mem", FileLocation(fpath, 2, 1)),
        TokenisedWord(Kind.INT, 1, FileLocation(fpath, 2, 5)),
        TokenisedWord(Kind.WORD, "+", FileLocation(fpath, 2, 7)),
        TokenisedWord(Kind.INT, 98, FileLocation(fpath, 2, 9)),
        TokenisedWord(Kind.WORD, ".", FileLocation(fpath, 2, 12)),
        TokenisedWord(Kind.WORD, "mem", FileLocation(fpath, 3, 1)),
        TokenisedWord(Kind.INT, 2, FileLocation(fpath, 3, 5)),
        TokenisedWord(Kind.WORD, "+", FileLocation(fpath, 3, 7)),
        TokenisedWord(Kind.INT, 99, FileLocation(fpath, 3, 9)),
        TokenisedWord(Kind.WORD, ".", FileLocation(fpath, 3, 12)),
    ]
    results = list(lex_file(fpath, [Path("stdlib")]))

    assert len(expected) == len(results), "lex_line didn't return the expected number of token."

    for res, exp in zip(results, expected):
        assert res == exp, "A token was wrong."


def test_lex_file_read_mem():
    fpath = "tests/input/read_mem.porth"
    expected = [
        TokenisedWord(Kind.WORD, "mem", FileLocation(fpath, 1, 1)),
        TokenisedWord(Kind.INT, 0, FileLocation(fpath, 1, 5)),
        TokenisedWord(Kind.WORD, "+", FileLocation(fpath, 1, 7)),
        TokenisedWord(Kind.INT, 97, FileLocation(fpath, 1, 9)),
        TokenisedWord(Kind.WORD, ".", FileLocation(fpath, 1, 12)),
        TokenisedWord(Kind.WORD, "mem", FileLocation(fpath, 2, 1)),
        TokenisedWord(Kind.INT, 1, FileLocation(fpath, 2, 5)),
        TokenisedWord(Kind.WORD, "+", FileLocation(fpath, 2, 7)),
        TokenisedWord(Kind.INT, 98, FileLocation(fpath, 2, 9)),
        TokenisedWord(Kind.WORD, ".", FileLocation(fpath, 2, 12)),
        TokenisedWord(Kind.WORD, "mem", FileLocation(fpath, 3, 1)),
        TokenisedWord(Kind.INT, 2, FileLocation(fpath, 3, 5)),
        TokenisedWord(Kind.WORD, "+", FileLocation(fpath, 3, 7)),
        TokenisedWord(Kind.INT, 99, FileLocation(fpath, 3, 9)),
        TokenisedWord(Kind.WORD, ".", FileLocation(fpath, 3, 12)),
        TokenisedWord(Kind.WORD, "mem", FileLocation(fpath, 5, 1)),
        TokenisedWord(Kind.INT, 0, FileLocation(fpath, 5, 5)),
        TokenisedWord(Kind.WORD, "+", FileLocation(fpath, 5, 7)),
        TokenisedWord(Kind.WORD, "dup", FileLocation(fpath, 5, 9)),
        TokenisedWord(Kind.WORD, ",", FileLocation(fpath, 5, 13)),
        TokenisedWord(Kind.INT, 1, FileLocation(fpath, 5, 15)),
        TokenisedWord(Kind.WORD, "+", FileLocation(fpath, 5, 17)),
        TokenisedWord(Kind.WORD, ".", FileLocation(fpath, 5, 19)),
        TokenisedWord(Kind.WORD, "mem", FileLocation(fpath, 6, 1)),
        TokenisedWord(Kind.INT, 1, FileLocation(fpath, 6, 5)),
        TokenisedWord(Kind.WORD, "+", FileLocation(fpath, 6, 7)),
        TokenisedWord(Kind.WORD, "dup", FileLocation(fpath, 6, 9)),
        TokenisedWord(Kind.WORD, ",", FileLocation(fpath, 6, 13)),
        TokenisedWord(Kind.INT, 1, FileLocation(fpath, 6, 15)),
        TokenisedWord(Kind.WORD, "+", FileLocation(fpath, 6, 17)),
        TokenisedWord(Kind.WORD, ".", FileLocation(fpath, 6, 19)),
        TokenisedWord(Kind.WORD, "mem", FileLocation(fpath, 7, 1)),
        TokenisedWord(Kind.INT, 2, FileLocation(fpath, 7, 5)),
        TokenisedWord(Kind.WORD, "+", FileLocation(fpath, 7, 7)),
        TokenisedWord(Kind.WORD, "dup", FileLocation(fpath, 7, 9)),
        TokenisedWord(Kind.WORD, ",", FileLocation(fpath, 7, 13)),
        TokenisedWord(Kind.INT, 1, FileLocation(fpath, 7, 15)),
        TokenisedWord(Kind.WORD, "+", FileLocation(fpath, 7, 17)),
        TokenisedWord(Kind.WORD, ".", FileLocation(fpath, 7, 19)),
    ]
    results = list(lex_file(fpath, [Path("stdlib")]))

    assert len(expected) == len(results), "lex_line didn't return the expected number of token."

    for index, (res, exp) in enumerate(zip(results, expected)):
        assert res == exp, f"Token {index} was wrong."


def test_load_file_infinite_macro():
    with raises(CompilerError) as excep:
        load_from_file("tests/input/infinite_macro.porth", [Path("stdlib"), Path("tests/input")], 128, 128)
    assert "tests/input/infinite_macro.porth:1:11: Macro 'bar' was expanded too many times (128 > 128)" in str(
        excep.value
    )


def test_load_file_infinite_include():
    with raises(CompilerError) as excep:
        load_from_file("tests/input/infinite_include.porth", [Path("stdlib"), Path("tests/input")], 128, 128)
    assert "infinite_include.porth:1:1: File 'infinite_include.porth' included too many times (128 > 128)" in str(
        excep.value
    )
