from pytest import raises

from porth.tokens import OP, FileLocation, Intrinsics, Keyword, Kind, Token


def test_kind():
    for enum, expected, rep in zip(
        [Kind.INT, Kind.WORD, Kind.STR, Kind.CHAR, Kind.KEYWORD],
        ["int", "word", "str", "char", "keyword"],
        ["<Kind.INT>", "<Kind.WORD>", "<Kind.STR>", "<Kind.CHAR>", "<Kind.KEYWORD>"],
    ):
        assert str(enum) == expected
        assert repr(enum) == rep


def test_intrinsics():
    values = [
        Intrinsics.PLUS,
        Intrinsics.MINUS,
        Intrinsics.MUL,
        Intrinsics.DIVMOD,
        Intrinsics.SHR,
        Intrinsics.SHL,
        Intrinsics.B_OR,
        Intrinsics.B_AND,
        Intrinsics.EQUAL,
        Intrinsics.GT,
        Intrinsics.LT,
        Intrinsics.LE,
        Intrinsics.GE,
        Intrinsics.NE,
        Intrinsics.DUP,
        Intrinsics.DROP,
        Intrinsics.SWAP,
        Intrinsics.OVER,
        Intrinsics.MEM,
        Intrinsics.LOAD,
        Intrinsics.STORE,
        Intrinsics.SYSCALL,
    ]
    expected = [
        "<Intrinsics.PLUS>",
        "<Intrinsics.MINUS>",
        "<Intrinsics.MUL>",
        "<Intrinsics.DIVMOD>",
        "<Intrinsics.SHR>",
        "<Intrinsics.SHL>",
        "<Intrinsics.B_OR>",
        "<Intrinsics.B_AND>",
        "<Intrinsics.EQUAL>",
        "<Intrinsics.GT>",
        "<Intrinsics.LT>",
        "<Intrinsics.LE>",
        "<Intrinsics.GE>",
        "<Intrinsics.NE>",
        "<Intrinsics.DUP>",
        "<Intrinsics.DROP>",
        "<Intrinsics.SWAP>",
        "<Intrinsics.OVER>",
        "<Intrinsics.MEM>",
        "<Intrinsics.LOAD>",
        "<Intrinsics.STORE>",
        "<Intrinsics.SYSCALL>",
    ]
    for value, exp in zip(values, expected):
        assert repr(value) == exp


def test_op():
    values = [
        OP.PUSH,
        OP.PUSH_STR,
        OP.DUMP,
        OP.INTRINSIC,
        OP.IF,
        OP.ELSE,
        OP.WHILE,
        OP.DO,
        OP.END,
    ]
    expected = [
        "<OP.PUSH>",
        "<OP.PUSH_STR>",
        "<OP.DUMP>",
        "<OP.INTRINSIC>",
        "<OP.IF>",
        "<OP.ELSE>",
        "<OP.WHILE>",
        "<OP.DO>",
        "<OP.END>",
    ]
    for value, exp in zip(values, expected):
        assert repr(value) == exp


def test_op_from_keyword():
    values = [
        Keyword.IF,
        Keyword.ELSE,
        Keyword.WHILE,
        Keyword.DO,
        Keyword.END,
    ]
    expected = [
        OP.IF,
        OP.ELSE,
        OP.WHILE,
        OP.DO,
        OP.END,
    ]
    for value, result in zip(values, expected):
        assert result == OP.from_keyword(value)


def test_op_from_keyword_errors():
    with raises(ValueError) as exp:
        OP.from_keyword(345)
    assert "Keyword '345' is not a valid operation." in str(exp)


def test_file_location_str():
    loc = FileLocation("toto.s", 10, 90)
    assert str(loc) == "toto.s:010:090"


def test_file_location_repr():
    loc = FileLocation("toto.s", 10, 90)
    assert repr(loc) == "toto.s:010:090"
